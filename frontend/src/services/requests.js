import api from "./api";

export async function getVideos(setData, checkAllWatched) {
  await api
    .get("/api/video/list")
    .then((res) => {
      setData(res.data);
      checkAllWatched(res.data);
      console.log(res.data, "vids");
    })
    .catch((err) => console.log(err));
}

export async function postVideosSpec(data) {
  await api
    .post("/api/video/update/compulsory", data)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => console.log(err));
}

export async function updateWatched(setVideoItems, data, checkAllWatched) {
  await api
    .post("/api/video/update/watched", {
      link: data.link,
    })
    .then((res) => {
      getVideos(setVideoItems, checkAllWatched);
      console.log(res);
    })
    .catch((err) => console.log(err));
}

export async function getSChoices(setData) {
  await api
    .get("/api/user/choices?specialisation=1")
    .then((res) => {
      setData(res.data);
      // console.log(res.data);
    })
    .catch((err) => console.log(err));
}

export async function getSubSChoices(setData) {
  await api
    .get("/api/user/choices?subspecialisation=1")
    .then((res) => {
      setData(res.data);
      // console.log(res.data);
    })
    .catch((err) => console.log(err));
}

export async function createUser(data, setUser) {
  await api
    .post("/api/user/create", data)
    .then((res) => {
      if ((res.status === 200) | (res.status === 201)) {
        console.log("User successfully created");
        setUser(res.data);
      }
    })
    .catch((err) => console.log(err));
}

export async function getUser(setData, setLoading) {
  await api
    .get("/api/user/get")
    .then((res) => {
      // console.log(res.data);
      setData(res.data);
      setLoading(false);
    })
    .catch((err) => {
      console.log(err);
      setLoading(false);
    });
}
