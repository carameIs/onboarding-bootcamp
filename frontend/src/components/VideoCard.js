import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import {
  Stack,
  Chip,
  Avatar,
  Badge,
  AvatarGroup,
  FormControlLabel,
} from "@mui/material";
import sada from "../assets/Images/andy_sad.png";
import sadj from "../assets/Images/jon_sad.png";
import sadk from "../assets/Images/ken_sad.png";
import happya from "../assets/Images/andy.png";
import happyj from "../assets/Images/jon.png";
import happyk from "../assets/Images/ken.png";
import { updateWatched } from "../services/requests";
import "../App.css";
import Austin from "../assets/Images/austin.svg";
import AustinSmile from "../assets/Images/austinsmile.svg";
import { Box } from "@mui/system";
import YouTube, { YouTubeProps } from "react-youtube";
import DoneIcon from "@mui/icons-material/Done";

const IndivAvatar = ({ img, type }) => {
  return <Avatar alt={type} src={img} sx={{ width: 60, height: 60 }} />;
};

const Sad = () => {
  return (
    <AvatarGroup>
      <IndivAvatar img={sada} type="sad" />
      <IndivAvatar img={sadj} type="sad" />
      <IndivAvatar img={sadk} type="sad" />
    </AvatarGroup>
  );
};

const Happy = () => {
  return (
    <AvatarGroup>
      <IndivAvatar img={happya} type="happy" />
      <IndivAvatar img={happyj} type="happy" />
      <IndivAvatar img={happyk} type="happy" />
    </AvatarGroup>
  );
};

export default function VideoCard({
  data,
  setVideoItems,
  sChoices,
  subSChoices,
  checkAllWatched,
}) {
  React.useEffect(() => {}, []);

  const _onReady = (event) => {
    // access to player in all event handlers via event.target
    console.log(event.target);
    event.target.stopVideo();
  };

  const _onEnd = (event) => {
    // access to player in all event handlers via event.target
    console.log("ended", event);
    updateWatched(setVideoItems, data, checkAllWatched);
    // event.target.pauseVideo();
  };

  const opts = {
    height: "195",
    width: "345",
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 1,
    },
  };

  function youtube_parser(url) {
    var regExp =
      /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return match && match[7].length == 11 ? match[7] : false;
  }

  return (
    <div style={{ margin: 50 }}>
      <Badge
        onClick={(e) => console.log(e)}
        badgeContent={data.watched ? <Happy /> : <Sad />}
      >
        <Card
          key={data.id}
          sx={{ maxWidth: 345 }}
          onClick={(e) => console.log(e.view.YT)}
        >
          <Box onClick={(e) => console.log(e)}>
            <YouTube
              videoId={youtube_parser(data.link)} //"lTTajzrSkCw"
              opts={opts}
              onReady={_onReady}
              onEnd={_onEnd}
            />
            {/* <CardMedia
            allow="fullscreen"
            component="iframe"
            image={data.link}
            ref={vidRef}
            id={vid_id}
            sx={{ cursor: "url(" + Austin + "), auto;" }}
          /> */}
          </Box>
          <CardContent sx={{ minHeight: 170 }}>
            <Typography gutterBottom variant="h5" component="div">
              {data.topic}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              textAlign="justify"
            >
              {data.description}
            </Typography>
          </CardContent>
          <CardActions sx={{ flex: 1, justifyContent: "space-between" }}>
            <Stack direction="row" spacing={1}>
              {sChoices.map((choice) =>
                choice.shorthand === data.type ? (
                  <Chip
                    label={choice.fullform}
                    sx={{ cursor: "url(" + Austin + "), auto;" }}
                    color={choice.shorthand === "BE" ? "primary" : "secondary"}
                  />
                ) : (
                  <></>
                )
              )}
              {subSChoices.map((choice) =>
                choice.shorthand === data.type ? (
                  <Chip
                    label={choice.fullform}
                    sx={{ cursor: "url(" + Austin + "), auto;" }}
                    color="warning"
                  />
                ) : (
                  <></>
                )
              )}
              {data.is_compulsory ? (
                <Chip
                  label="Compulsory"
                  color="error"
                  variant="outlined"
                  sx={{ cursor: "url(" + Austin + "), auto;" }}
                />
              ) : (
                <></>
              )}
            </Stack>
            {data.watched ? (
              <Chip
                icon={<DoneIcon />}
                label="Watched"
                color="success"
                variant="outlined"
                sx={{ cursor: "url(" + Austin + "), auto;" }}
              />
            ) : (
              <></>
            )}
          </CardActions>
        </Card>
      </Badge>
    </div>
  );
}
