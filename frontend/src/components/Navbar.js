import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { MenuItems } from "./MenuItems";
import MenuIcon from "@mui/icons-material/Menu";
import CloseIcon from "@mui/icons-material/Close";
import logo from "../assets/Images/Reluvate.png";
import "../css/Navbar.css";
import "../App.css";

const Navbar = (props) => {
  const { user } = props;
  const [clicked, setClicked] = useState(false);

  const handleClick = () => {
    setClicked(!clicked);
  };

  return (
    <div className="NavBar">
      <nav className="NavBarItems">
        <a href="/">
          <img src={logo} alt="Reluvate logo" className="navlink-logo pointer"></img>
        </a>
        <div className="menu-icon" onClick={handleClick}>
          {clicked ? <CloseIcon /> : <MenuIcon />}
        </div>
        {Object.keys(user).length > 0 ? (
          <ul className={clicked ? "nav-menu active" : "nav-menu"}>
            {MenuItems.map((item, index) => {
              return (
                <li key={index}>
                  <Link className={item.cName} to={item.url}>
                    {item.title}
                  </Link>
                </li>
              );
            })}
          </ul>
        ) : (
          <></>
        )}
      </nav>
    </div>
  );
};

export default Navbar;
