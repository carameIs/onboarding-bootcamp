export const MenuItems = [
  {
    title: "Home",
    url: "/",
    cName: "nav-links pointer",
  },
  {
    title: "Tutorials",
    url: "/tutorials",
    cName: "nav-links pointer",
  },
  {
    title: "Journal",
    url: "/journal",
    cName: "nav-links pointer",
  },
];
