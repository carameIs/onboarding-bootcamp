import "./App.css";
import { useState, useEffect } from "react";
import Videos from "./pages/Videos";
import Home from "./pages/Home";
import Form from "./pages/Form";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { getUser } from "./services/requests";
import Navbar from "./components/Navbar";
import Journal from "./pages/Journal";

function App() {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getUser(setUser, setLoading);
  }, []);

  return (
    <div>
      {Object.keys(user).length > 0 && !loading ? (
        <Router>
          <Navbar user={user} />
          <Routes>
            <Route path="*" element={<Home />} />
            <Route path="/tutorials" element={<Videos />} />
            <Route path="/journal" element={<Journal />}/>
          </Routes>
        </Router>
      ) : !loading ? (
        <Router>
          <Navbar user={user} />
          <Routes>
            <Route path="*" element={<Form user={user} setUser={setUser} />} />
          </Routes>
        </Router>
      ) : (
        <></>
      )}
    </div>
  );
}

export default App;
