import React, { useEffect, useState } from "react";
import {
  Typography,
  Stack,
  TextField,
  FormGroup,
  FormControlLabel,
  Button,
  Radio,
  FormControl,
  FormLabel,
  Box,
} from "@mui/material";
import {
  getSChoices,
  getSubSChoices,
  createUser,
  postVideosSpec,
} from "../services/requests";
import "../App.css";
import Austin from "../assets/Images/austin.svg";
import AustinSmile from "../assets/Images/austinsmile.svg";

const Form = (props) => {
  const { user, setUser } = props;
  const [name, setName] = useState("");
  const [sChoices, setSChoices] = useState([]);
  const [subSChoices, setsubSChoices] = useState([]);
  const [sChoicesSelected, setSChoicesSelected] = useState("");
  const [subSChoicesSelected, setsubSChoicesSelected] = useState("");

  useEffect(() => {
    getSChoices(setSChoices);
    getSubSChoices(setsubSChoices);
  }, []);

  const handleSubmit = () => {
    let data = {
      name: name,
      specialisation: sChoicesSelected,
      subspecialisation: subSChoicesSelected,
    };
    // console.log(data);
    createUser(data, setUser);
    postVideosSpec({
      selection: [sChoicesSelected, subSChoicesSelected],
    });
  };

  return (
    <div style={{ width: "90%", margin: "auto" }}>
      <Typography variant="h4" sx={{ my: 3 }}>
        Welcome to Reluvate!
      </Typography>
      <Typography variant="h5" sx={{ my: 3 }}>
        Enter your name and specialisation(s) to view a series of tutorials
        specially curated for you!
      </Typography>
      <FormControl>
        <Stack spacing={2}>
          <FormLabel>Name</FormLabel>
          <TextField
            size="small"
            onChange={(e) => {
              setName(e.target.value);
            }}
            inputProps={{ className: "pointer" }}
            autoComplete="off"
          />
          <FormLabel>Specialisation</FormLabel>
          <FormGroup row>
            {sChoices.map(
              (item) =>
                item && (
                  <FormControlLabel
                    control={
                      <Radio
                        checked={sChoicesSelected.includes(item.shorthand)}
                        onChange={(event) => {
                          setSChoicesSelected(item.shorthand);
                        }}
                        sx={{ cursor: "url(" + AustinSmile + "), auto;" }}
                      />
                    }
                    value={item.shorthand}
                    label={item.fullform}
                    key={item.shorthand}
                  />
                )
            )}
          </FormGroup>
          <FormLabel>Sub-specialisation</FormLabel>
          <FormGroup row>
            {subSChoices.map(
              (item) =>
                item && (
                  <FormControlLabel
                    control={
                      <Radio
                        checked={subSChoicesSelected.includes(item.shorthand)}
                        onChange={(event) => {
                          setsubSChoicesSelected(item.shorthand);
                        }}
                        sx={{ cursor: "url(" + AustinSmile + "), auto;" }}
                      />
                    }
                    value={item.shorthand}
                    label={item.fullform}
                    key={item.shorthand}
                  />
                )
            )}
          </FormGroup>
          <Button
            variant="contained"
            onClick={() => handleSubmit()}
            sx={{ cursor: "url(" + AustinSmile + "), auto;" }}
          >
            Submit
          </Button>
          <Box sx={{ height: "20px" }} />
        </Stack>
      </FormControl>
    </div>
  );
};
export default Form;
