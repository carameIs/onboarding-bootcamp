import { useEffect, useState, useRef, forwardRef } from "react";
import { getVideos } from "../services/requests";
import { Stack, Box, Card, Typography } from "@mui/material";
import VideoCard from "../components/VideoCard";
import congratulations from "../assets/Images/Congratulations.jpeg";
import Zoom from "@mui/material/Zoom";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import { getSChoices, getSubSChoices } from "../services/requests";

const Videos = (props) => {
  const { user } = props;
  const [videoItems, setVideoItems] = useState([]);
  const [isAllCompulsoryWatched, setIsAllCompulsoryWatched] = useState(false);
  const [open, setOpen] = useState(false);
  const [sChoices, setSChoices] = useState([]);
  const [subSChoices, setsubSChoices] = useState([]);
  let container = [];

  const Transition = forwardRef(function Transition(props, ref) {
    return <Zoom in={isAllCompulsoryWatched} ref={ref} {...props} />;
  });
  const handleClose = () => {
    setOpen(false);
  };

  const checkAllWatched = (data) => {
    if (data.length > 0) {
      for (let item of data) {
        if (item.watched) {
          container.push(item);
        }
      }
      if (container.length === data.length) {
        console.log("all watched");
        setIsAllCompulsoryWatched(true);
        setOpen(true);
      }
    }
  };

  useEffect(() => {
    getVideos(setVideoItems, checkAllWatched);
    getSChoices(setSChoices);
    getSubSChoices(setsubSChoices);
  }, []);
  return (
    <div>
      <Stack
        fullWidth
        sx={{
          backgroundColor: "#0a204a",
          color: "#FFFFFF",
          flex: 1,
          alignItems: "center",
          paddingBottom: 2,
        }}
      >
        {isAllCompulsoryWatched ? (
          <Typography variant="h5">
            Congratulations! You have watched all compulsory videos :)
          </Typography>
        ) : (
          <Typography variant="h5">
            Make them smile, watch all compulsory videos
          </Typography>
        )}
      </Stack>
      <Box sx={{ flexGrow: 1 }}>
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          {videoItems &&
            videoItems.map((card) => (
              <Grid item xs={2} sm={4} md={4} key={card.link}>
                <VideoCard
                  data={card}
                  setVideoItems={setVideoItems}
                  sChoices={sChoices}
                  subSChoices={subSChoices}
                  checkAllWatched={checkAllWatched}
                  id="videoCards"
                />
              </Grid>
            ))}
        </Grid>
      </Box>

      {/* <Stack direction="row"></Stack> */}
      {isAllCompulsoryWatched && open ? (
        <Dialog
          open={open}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleClose}
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle sx={{ textAlign: "center" }}>
            Congratulations! You have watched all compulsory videos :)
          </DialogTitle>
          <img src={congratulations} style={{ height: "auto", width: 600 }} />
        </Dialog>
      ) : (
        <></>
      )}
    </div>
  );
};

export default Videos;
