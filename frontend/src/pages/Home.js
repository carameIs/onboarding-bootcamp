import React, { useEffect, useState, useRef } from "react";
import { Typography, Tabs, Tab, Box, TextField } from "@mui/material";
import { getUser } from "../services/requests";

const Home = () => {
  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getUser(setData, setLoading);
  }, []);

  return (
    <Box
      sx={{
        width: "90%",
        margin: "auto",
      }}
    >
      {data && (
        <Typography variant="h5" sx={{ my: 3 }}>
          Welcome to Reluvate, {data.name}!
        </Typography>
      )}
    </Box>
  );
};
export default Home;
