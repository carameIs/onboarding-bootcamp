# Reluvate Onboarding Bootcamp

## Welcome to Reluvate!
A warm welcome to joining Reluvate! Congratulations on passing the technical assessments and interviews, and we thank you for being a part of our team! To ease your journey into Reluvate, we have prepared a series of tasks for the upcoming week. For your first day, your tasks are as follows:
1) Clone this repository to your local machine.
2) Set up the backend and frontend for running locally.
3) Enjoy the tutorials we have lined up for you.

You are encouraged to continue doing further reading after finishing your tasks for today. Do remember to set up a sharing session with your Team Leader at the end of the day. 

## Setting up Django backend
### Installing dependencies
1) Set up a virtual environment in the directory of your choice.
2) For Windows OS: run `<path to environment>\Scripts\activate`, for Linux OS: run `source <path to environment>/env/bin/activate` to activate your virtual environment.
3) Change directory to where requirements.txt is located.
4) Run `pip install -r requirements.txt` to install dependencies into your virtual environment.
### Migration and population of database
1) Change directory to where manage.py is located.
2) Run `python manage.py makemigrations <app name>` to make migration files for each app.
3) Run `python manage.py migrate` to apply migrations to the database.
4) Run `python manage.py populate_videos` to populate the database.
### Running the server
1) Run `python manage.py runserver` to run the backend locally.
2) Backend API should be accessible in your browser via http://127.0.0.1:8000/.

## Setting up React frontend
### Installing dependencies
1) For Windows OS: run `<path to environment>\Scripts\activate`, for Linux OS: run `source <path to environment>/env/bin/activate` to activate your virtual environment.
2) Run `pip install nodeenv` to install nodeenv.
3) Run `nodeenv nenv --node 16.13.0` to setup nodeenv using Node version 16.13.0 in the directory of your choice.
4) For Windows OS: run `<path to environment>\Scripts\activate`, for Linux OS: run `source <path to environment>/env/bin/activate` to activate your nodeenv environment.
5) Change directory to where package.json is located.
6) Run `npm install` to install dependencies into your nodeenv environment.
### Running the server
1) Run `npm start` to run the frontend locally.
2) React frontend should show up in your browser via http://localhost:3000/.