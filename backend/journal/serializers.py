from rest_framework import serializers
from journal.models import JournalEntry
from django.utils import timezone

class JournalEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = JournalEntry
        fields = '__all__'
        optional_fields = ('create_at', 'update_at')

    def create(self, validated_data):
        new_entry = JournalEntry.objects.create(**validated_data)
        return new_entry

    def update(self, instance, validated_data):
        validated_data['update_at'] = timezone.now()
        super(JournalEntrySerializer, self).update(instance, validated_data)
        return instance


class JournalEntryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = JournalEntry
        fields = ('id', 'title', 'update_at')