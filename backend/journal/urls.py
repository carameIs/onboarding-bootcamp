from django.urls import include, path
from journal.views import JournalListView, JournalEntryView

urlpatterns = [
    path('list', JournalListView.as_view()),
    path('create', JournalEntryView.Create.as_view()),
    path('<int:pk>', JournalEntryView.Get.as_view()),
    path('<int:pk>/update', JournalEntryView.Update.as_view()),
]