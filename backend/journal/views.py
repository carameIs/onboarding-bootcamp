from django.shortcuts import get_object_or_404, render
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from journal.models import JournalEntry
from journal.serializers import JournalEntrySerializer, JournalEntryListSerializer

class JournalListView(generics.ListAPIView):
    queryset = JournalEntry.objects.all()
    serializer_class = JournalEntryListSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class JournalEntryView(APIView):

    class Get(generics.RetrieveAPIView):
        queryset = JournalEntry.objects.all()
        serializer_class = JournalEntrySerializer
        lookup_field = 'pk'

    class Create(generics.CreateAPIView):
        serializer_class = JournalEntrySerializer

        def post(self, request, format=None):
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    class Update(generics.UpdateAPIView):
        queryset = JournalEntry.objects.all()
        serializer_class = JournalEntrySerializer

        def update(self, request, *args, **kwargs):
            entry = get_object_or_404(JournalEntry, pk=kwargs['pk'])
            return super(JournalEntryView.Update, self).update(request, *args, **kwargs)