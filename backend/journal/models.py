from django.db import models
from django.utils import timezone

class JournalEntry(models.Model):
    title = models.CharField(max_length=500, default="No title")
    body = models.TextField(default="No content.")
    create_at = models.DateTimeField(default=timezone.now, blank=True)
    update_at = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return f'[{self.id}] {self.title}'