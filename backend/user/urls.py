from django.urls import include, path
from user.views import UserView, ChoicesView

urlpatterns = [
    path('get', UserView.Get.as_view()),
    path('create', UserView.Create.as_view()),
    path('choices', ChoicesView.as_view()),
]