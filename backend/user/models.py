from django.db import models
from django.utils import timezone

class User(models.Model):
    # Set choice fields
    SPECIALISATION = [
        ('BE', 'Backend'),
        ('FE', 'Frontend'),
    ]
    SUBSPECIALISATION = [
        ('TDD', 'Unit testing'),
        ('ML', 'Machine Learning'),
    ]

    # Set fields
    name = models.CharField(max_length=20)
    specialisation = models.CharField(max_length=2, choices=SPECIALISATION)
    subspecialisation = models.CharField(max_length=3, choices=SUBSPECIALISATION)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name
