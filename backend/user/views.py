from django.shortcuts import render
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from user.models import User
from user.serializers import UserSerializer, CreateUserSerializer

class UserView(APIView):

    class Get(generics.ListAPIView):
        serializer_class = UserSerializer
        queryset = User.objects.all()

        def get(self, request, *args, **kwargs):
            if len(self.get_queryset()):
                serializer = self.get_serializer(self.get_queryset()[0])
                return Response(serializer.data)
            else:
                return Response('No user found, please enter create user page.', status=status.HTTP_503_SERVICE_UNAVAILABLE)

    class Create(generics.CreateAPIView):
        serializer_class = CreateUserSerializer
        
        def post(self, request, *args, **kwargs):
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChoicesView(APIView):

    def get(self, request, *args, **kwargs):
        res = []
        choices = []
        if int(request.GET.get('specialisation', 0)):
            choices = User.SPECIALISATION
        elif int(request.GET.get('subspecialisation', 0)):
            choices = User.SUBSPECIALISATION

        for shorthand, fullform in choices:
            res.append({'shorthand': shorthand, 'fullform': fullform})
        return Response(res)


