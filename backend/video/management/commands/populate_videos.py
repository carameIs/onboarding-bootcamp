from lib2to3.pytree import Base
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from video.models import Video
import json

class Command(BaseCommand):
    help = 'Populates video tutorials based on json given.'

    def handle(self, *args, **options):
        try:
            print('Clearing out old videos')
            Video.objects.all().delete()

            print('Getting new videos')
            json_path = "./video/management/commands/videos.json"
            with open(json_path, 'r') as f:
                videos = json.load(f)
            
            print('Adding videos to db')
            with transaction.atomic():
                for video in videos:
                    Video.objects.create(
                        is_compulsory=video['is_compulsory'],
                        type=video['type'],
                        topic=video['topic'],
                        link=video['link'],
                        description=video['description'],
                    )
            
            self.stdout.write(self.style.SUCCESS('Videos successfully added.'))

        except Exception as e:
            print(e)
            raise CommandError('Videos were not successfully added.')