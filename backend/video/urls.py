from django.urls import include, path
from video.views import UpdateCompulsoryStatus, UpdateWatchedStatus, VideoView

urlpatterns = [
    path('list', VideoView.as_view()),
    path('update/compulsory', UpdateCompulsoryStatus.as_view()),
    path('update/watched', UpdateWatchedStatus.as_view()),
]