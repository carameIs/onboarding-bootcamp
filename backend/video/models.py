from django.db import models

class Video(models.Model):
    # Set choice fields
    TYPE = [
        ('BE', 'Backend'),
        ('FE', 'Frontend'),
        ('TDD', 'Unit Testing'),
        ('ML', 'Machine Learning'),
    ]
    # Set fields
    is_compulsory = models.BooleanField(default=False)
    type = models.CharField(max_length=3, choices=TYPE)
    topic = models.CharField(max_length=1000)
    watched = models.BooleanField(default=False)
    link = models.CharField(max_length=1000)
    description = models.CharField(max_length=2000)

    def __str__(self):
        return self.topic
