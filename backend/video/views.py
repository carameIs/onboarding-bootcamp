from django.shortcuts import render
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from video.models import Video
from video.serializers import VideoSerializer

class VideoView(generics.ListAPIView):
    queryset = Video.objects.filter(is_compulsory=True)
    serializer_class = VideoSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()

        choices = list(type[1] for type in Video.TYPE)
        for choice in choices:
            if int(request.GET.get(choice.lower(), 0)):
                queryset = queryset.filter(type=choice)
                break

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class UpdateCompulsoryStatus(APIView):

    def post(self, request, *args, **kwargs):
        selection = request.data['selection'] #array
        videos = Video.objects.filter(type__in=selection)
        for video in videos:
            video.is_compulsory = True
            video.save()
        return Response(f'Compulsory status updated for videos with type in {selection}', status=status.HTTP_200_OK)


class UpdateWatchedStatus(APIView):

    def post(self, request, *args, **kwargs):
        link = request.data['link'] #text
        videos = Video.objects.filter(link=link)
        for video in videos:
            video.watched = True
            video.save()
        return Response(f'Watched status updated for video with link {link}', status=status.HTTP_200_OK)